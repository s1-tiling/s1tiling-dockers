#!/bin/bash
#
# =========================================================================
#
#   Copyright 2021-2024 (c) CS Group France. All rights reserved.
#
#   This file is part of S1Tiling project
#       https://gitlab.orfeo-toolbox.org/s1-tiling/s1tiling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# =========================================================================
# This particular file is a fork of OTB9 post_install.sh file, tuned for
# the needs of building S1TIling dockers.

function _execute()
{
    _verbose "$@"
    [ "${dryrun:-0}" = "1" ] || "$@"
}

__log_head='$> '
function _verbose()
{
    # if debug...
    echo -e "\n${__log_head:-}$*"
    # echo "\n${__log_head:-}$*"
    # fi
}

function _xargs()
{
    _verbose xargs "$@"
    xargs "$@"
}


echo "***** First time launching OTB after installation, doing some post installation steps before use *****"
# Apply necessary patches for a modular install because cmake generates these file at configure time, not at packaging time
_execute sed -i "s/FATAL_ERROR/WARNING/g" "${OTB_INSTALL_DIR}/lib/cmake/OTB-9.0/OTBTargets.cmake"
_execute sed -i "s/FATAL_ERROR/WARNING/g" "${OTB_INSTALL_DIR}/lib/cmake/OTB-9.0/OTBModuleAPI.cmake"

find "${OTB_INSTALL_DIR}/lib/cmake" -name "*.cmake" | _xargs sed -i 's#/builds/otb/xdk#${OTB_INSTALL_PREFIX}#g'
# sed -i 's#/builds/otb/xdk#${OTB_INSTALL_DIR}/g' "${OTB_INSTALL_DIR}/bin/gdal-config"
_execute sed -i 's#/builds/otb/xdk#${OTB_INSTALL_DIR}#g' "${OTB_INSTALL_DIR}/bin/curl-config"
_execute sh "${OTB_INSTALL_DIR}"/tools/sanitize_rpath.sh

## # Check python version, if python 3.10 (ubuntu 22 and debian 12) download and extract the gdal bindings for python 3.10
## pyversion="$(python3 -V 2>&1 | sed 's/.* \([0-9]\).\([0-9]*\).*/\1\2/')"
## if [ "$pyversion" = "310" ]; then
##     echo "*** Python 3.10 detected, downloading gdal bindings compiled for python 3.10 ***"
##     _execute curl https://www.orfeo-toolbox.org/packages/archives/OTB/OTB-GDAL-bindings-py310.tar.gz -o "${OTB_INSTALL_DIR}"/tools/py310.tar.gz
##     _execute tar -xf "${OTB_INSTALL_DIR}/tools/py310.tar.gz" -C "${OTB_INSTALL_DIR}/lib/python3/dist-packages/osgeo/"
##     _execute rm "${OTB_INSTALL_DIR}/tools/py310.tar.gz"
##     echo "*** GDAL bindings for python 3.10 successfully installed ***"
## fi

# Force the recompilation in the right place (virtenv)
_execute ctest -S "${OTB_INSTALL_DIR}/share/otb/swig/build_wrapping.cmake" -VV

echo "OK" > "${OTB_INSTALL_DIR}"/tools/install_done.txt
