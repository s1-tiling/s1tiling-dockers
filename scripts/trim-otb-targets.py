#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#
#   Copyright 2021-2024 (c) CS Group France. All rights reserved.
#
#   This file is part of S1Tiling project
#       https://gitlab.orfeo-toolbox.org/s1-tiling/s1tiling
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Luc HERMITTE     (CS Group France)
#
# =========================================================================

import os
import re
import sys
from typing import List

def read_excluded_targets(filename: str) -> List[str]:
    with open(filename, 'r') as f:
        targets = f.readlines()
        return [t.rstrip() for t in targets]


def is_comment(line: str) -> bool:
    return (len(line) > 0) and (line[0] == '#')

def trim_target_references(src: str, dst: str, targets: List[str]) -> None:
    matcher = re.compile(r'# Import target "(' + '|'.join(targets) + r')"')

    print(f"matching with #{len(targets)} targets: {matcher}")

    with open(src, 'r') as f_src:
        lines = f_src.readlines()

    res = []
    keep = True
    for line in lines:
        if not keep:
            if not is_comment(line) or matcher.match(line):
                continue
            keep = True
        elif matcher.match(line):
            keep = False
            continue
        res.append(line)

    with open(dst, 'w') as f_dst:
        f_dst.writelines(res)

if __name__ == '__main__':
    # $1: list of excluded OTB targets
    # $2: path to OTBTargets-release.cmake
    # $3: path to output that replaces OTBTargets-release
    # TODO: add checks!
    f_tgt = sys.argv[1]
    f_src = sys.argv[2]
    f_dst = sys.argv[3]
    print(f"Remove {f_tgt!r} from {f_src!r}. And write back the result into {f_dst!r}.")
    if not os.path.exists(f_tgt):
        print(f"Cannot read targets to remove from non existant {f_tgt!r} file")
        sys.exit(1)
    if not os.path.exists(f_src):
        print(f"Cannot remove targets from non existant {f_src!r} file")
        sys.exit(1)
    if not os.path.exists(f_dst):
        print(f"Cannot remove targets to non existant {f_dst!r} file")
        sys.exit(1)
    targets = read_excluded_targets(f_tgt)
    trim_target_references(f_src, f_dst, targets)
