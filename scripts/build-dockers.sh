#!/bin/bash
# =========================================================================
#
#   Copyright 2021-2024 (c) CS Group France. All rights reserved.
#
#   This file is part of S1Tiling project
#       https://gitlab.orfeo-toolbox.org/s1-tiling/s1tiling
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Luc HERMITTE     (CS Group France)
#
# =========================================================================

# Fail at first error
set -e

## ======[ Globals {{{1
# ==[ Colors {{{2
declare -A _colors
_colors[reset]="$(tput sgr0)"
_colors[red]="$(tput setaf 1)"
_colors[green]="$(tput setaf 2)"
_colors[yellow]="$(tput setaf 3)"
_colors[blue]="$(tput setaf 4)"
_colors[magenta]="$(tput setaf 5)"
_colors[cyan]="$(tput setaf 6)"
_colors[white]="$(tput setaf 7)"
_colors[bold]="$(tput bold)"
_colors[blink]="$(tput blink)"

_colors[path]="${_colors[green]}"
_colors[install_path]="${_colors[path]}"

## ======[ Support functions {{{1

# ==[ _ask_yes_no            {{{2
function _ask_Yes_no()
{
    local prompt="$1"
    local response
    read -r -p "${prompt} [Y/O/n] " response
    response=${response,,}    # tolower
    [[ $response =~ ^(yes|y|o|oui|)$ ]]
}

# ==[ _belongs               {{{2
# http://stackoverflow.com/a/15394738/15934
# This comment and code Copyleft, selectable license under the GPL2.0 or
# later or CC-SA 3.0 (CreativeCommons Share Alike) or later. (c) 2008.
# All rights reserved. No warranty of any kind. You have been warned.
# http://www.gnu.org/licenses/gpl-2.0.txt
# http://creativecommons.org/licenses/by-sa/3.0/
function _belongs()
{
    local value="$1"
    shift
    [[ " $@ " =~ " ${value} " ]]
}

# ==[ _current_dir           {{{2
_current_dir() {
    (cd "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")" > /dev/null && pwd)
}

# ==[ _die                   {{{2
function _die()
{
   local msg=$1
   [ -z "${msg}" ] && msg="Died"
   # echo "BASH_SOURCE: ${#BASH_SOURCE[@]}, BASH_LINENO: ${#BASH_LINENO[@]}, FUNCNAME: ${#FUNCNAME[@]}"
   printf "${BASH_SOURCE[0]:-($0 ??)}:${BASH_LINENO[0]}: ${FUNCNAME[1]}: ${msg}\n" >&2
   for i in $(seq 2 $((${#BASH_LINENO[@]} -1))) ; do
       printf "called from: ${BASH_SOURCE[$i]:-($0 ??)}:${BASH_LINENO[$(($i-1))]}: ${FUNCNAME[$i]}\n" >&2
   done
   # printf "%s\n" "${msg}" >&2
   exit 127
}

# ==[ _execute               {{{2
function _execute()
{
    _verbose "$@"
    [ "${dryrun:-0}" = "1" ] || "$@"
}

# ==[ _expect_file           {{{2
# Usage:
# - _expect_file ${filename}   # takes a filename
function _expect_file()
{
    [ $# -gt 0 ] || _die "ERROR: too few parameters to _expect_file"
    filename="$1"
    info="'${filename}'"
    _verbose "Need '${filename}'"
    local msg
    if [ $# -lt 2 ] ; then
        msg="ERROR directory '${filename}' doesn't exist."
    else
        msg="$2"
    fi
    [ "${dryrun:-0}" = "1" ] || [ -f "${filename}" ] || _die "${msg}"
}

# ==[ _extract_OTB_version   {{{2
function _extract_OTB_version()
{
    version="$(echo "${1}" | sed 's#.*OTB-\([0-9][^-_]*\).*#\1#')"
    echo "${version}"
}

# ==[ _git_tag               {{{2
function _git_tag()
{
    git tag
}

# ==[ _git_sha               {{{2
function _git_sha()
{
    git rev-parse --short HEAD
}

# ==[ _is_set                {{{2
function _is_set()
{
    # [[ -v $1 ]] with bash 4.2+
    [[ -n ${!1+x} ]]
}

# ==[ _is_true               {{{2
function _is_true()
{
    _is_set $1 && [[ $1 -eq 1 ]]
}

# ==[ _remove_directory      {{{2
function _remove_directory()
{
    local d="$1"
    [ -d "$d" ] || _die "Error: '$d' does not exist"
    _execute rm -r "${d}"
}

# ==[ _replace_in_array      {{{2
# $1: pattern
# $2: new value
# $3+ array to change
function _replace_in_array()
{
    local pat="$1"
    local repl="$2"
    shift
    shift
    for e in "$@"; do
        [[ "${e}" =~ "${pat}" ]] && echo "${repl}" || echo "${e}"
    done
}

# ==[ _split_by              {{{2
function _split_into_by()
{
    local res="$1"
    local IFS="$2"
    read -r -a "${res}" <<< "$3"
}

# ==[ _clean_tmp_dir         {{{2
function _clean_tmp_dir()
{
    echo ""
    echo "Cleaning temporary working directory ${tmp_wrk_dir}:"
    _is_set tmp_wrk_dir || _die "Error: no 'tmp_wrk_dir' variable"
    _remove_directory "${tmp_wrk_dir}"
}

# ==[ _verbose               {{{2
__log_head='\033[36m$>\033[0m '
function _verbose()
{
    # if debug...
    echo -e "\n${__log_head:-}$*"
    # fi
}

## ======[ Restore colors, in all cases {{{1
function _restore_colors
{
    echo -en "${_colors[reset]}"
}
trap _restore_colors EXIT

## ======[ Main program {{{1
# ==[ Usage                      {{{2
usage() {
    echo "USAGE: $0 [OPTIONS] /path/to/OTB-?.?.?-Linux64.run"
    echo ""
    echo "OPTIONS"
    echo " --dryrun"
    echo " --os         <ubuntu>|fedora"
    echo " --type       <all>|base,dev,user,<directory>"
    echo "              base       : root docker"
    echo "              dev        : docker used by unit tests"
    echo "              user       : s1tiling docker for end-user, obtained from pip"
    echo "              <directory>: directory to s1tiling sources"
    echo " --repository <registry.orfeo-toolbox.org/s1-tiling/s1tiling-dockers>"
    echo " --version    <s1tiling-version>     -- used by user stage"

    [ -z "$1" ] || {
        echo
        echo "$1"
        exit 127
    }
}

if [ $# -lt 1 ] ; then
    usage
    exit -1
fi

# ==[ Parse parameters           {{{2
os=ubuntu
types=all
repository="registry.orfeo-toolbox.org/s1-tiling/s1tiling-dockers"
while [ $# -gt 0 ] ; do
    case $1 in
        --dryrun|-n)
            dryrun=1
            ;;
        --os)
            os="$2"
            shift
            ;;
        --type|-t)
            types="$2"
            shift
            ;;
        --repository|-r)
            repository="$2"
            shift
            ;;
        --version)
            S1TILING_VERSION="$2"
            shift
            ;;
        *)
            OTB_package="$1"
            ;;
    esac
    shift
done

if [ "${types}" == "all" ] ; then
    types="base,dev,user"
fi

declare -a type_list
_split_into_by type_list ',' "${types}"
# the steps have to be ordered base < dev, base < user
type_list=($(printf "%s\n" "${type_list[@]}" | sort))

# ==[ Check parameter validity {{{2
root_dir="$(dirname "$(_current_dir)")"
[ -d "${root_dir}/${os}" ] || _die "Error: No Dockerfiles known for the specified OS:'${os}' in '${root_dir}'"
[ -f "${OTB_package}" ]    || _die "Error: Specified OTB package file does not exist"

user_dir=""
for step in "${type_list[@]}" ; do
    if [ -d "${step}" ] ; then
        user_dir="${step}"
    else
        [ -d "${root_dir}/${os}/${step}" ] || _die "Error: No Dockerfiles known for the specified step:'${step}' in '${root_dir}/${os}'"
    fi
done

# Extract OTB version
export OTB_VERSION="$(_extract_OTB_version "${OTB_package}")"

if [[ ${OTB_VERSION} =~ ^7 ]] ; then
    PYTHON_VERSION=3.8
    OS_VERSION=20.04  # Yes this is ubuntu specific...
else
    PYTHON_VERSION=3.10
    OS_VERSION=22.04
fi

# Define the parameters
LIA_VERSION=${LIA_VERSION:-tags/1.1.0rc1}
GAMMA_AREA_VERSION=${GAMMA_AREA_VERSION:-tags/1.2.0rc1}
S1TD_VERSION=${S1TD_VERSION:-1.4.0}
OTB_INSTALLER="$(basename "${OTB_package}")"
FULL_VERSION="${S1TD_VERSION}-${os}${OS_VERSION}-otb${OTB_VERSION}"

declare -a args=()
args+=(--build-arg CI_COMMIT_SHA="$(_git_sha)")
args+=(--build-arg OTB_VERSION="${OTB_VERSION}")
if [ "${OTB_INSTALLER/Core/}" != "$OTB_INSTALLER" ] ; then
    # OTB 9: multiple archives
    OTB_ARCHIVE_CORE="${OTB_INSTALLER}"
    OTB_ARCHIVE_DEPENDENCIES="${OTB_INSTALLER/Core/Dependencies}"
    OTB_ARCHIVE_SAR="${OTB_INSTALLER/Core/Sar}"
    OTB_ARCHIVE_FEATURE_EXTRACTION="${OTB_INSTALLER/Core/FeaturesExtraction}"
    _expect_file "${OTB_ARCHIVE_CORE}"
    _expect_file "${OTB_ARCHIVE_DEPENDENCIES}"
    _expect_file "${OTB_ARCHIVE_SAR}"
    _expect_file "${OTB_ARCHIVE_FEATURE_EXTRACTION}"
    args+=(--build-arg OTB_ARCHIVE_CORE="${OTB_ARCHIVE_CORE}")
    args+=(--build-arg OTB_ARCHIVE_DEPENDENCIES="${OTB_ARCHIVE_DEPENDENCIES}")
    args+=(--build-arg OTB_ARCHIVE_SAR="${OTB_ARCHIVE_SAR}")
    args+=(--build-arg OTB_ARCHIVE_FEATURE_EXTRACTION="${OTB_ARCHIVE_FEATURE_EXTRACTION}")
    _install_v9=1
    DOCKERFILE_NAME_base=Dockerfile9
else # OTB 7, 8: single run file
    args+=(--build-arg OTB_INSTALLER="${OTB_INSTALLER}")
    DOCKERFILE_NAME_base=Dockerfile
fi
args+=(--build-arg S1TD_VERSION="${S1TD_VERSION}")
args+=(--build-arg FULL_VERSION="${FULL_VERSION}")
args+=(--build-arg OS="${os}")
args+=(--build-arg OS_VERSION="${OS_VERSION}")
args+=(--build-arg PYTHON_VERSION="${PYTHON_VERSION}")
args+=(--build-arg CREATION_DATE="$(date -u '+%Y-%m-%dT%T%z')")

echo "Parameters detected:"
echo "- OS:               ${os}"
echo "- OS version:       ${OS_VERSION}"
echo "- Types:            ${type_list[@]}"
echo "- OTB file:         ${OTB_package}"
if _is_true _install_v9 ; then
    echo "- OTB dependencies: ${OTB_ARCHIVE_DEPENDENCIES}"
    echo "- OTB core:         ${OTB_ARCHIVE_CORE}"
    echo "- OTB sar:          ${OTB_ARCHIVE_SAR}"
    echo "- OTB feature extr.:${OTB_ARCHIVE_FEATURE_EXTRACTION}"
    OTB_ARCHIVES=("${OTB_ARCHIVE_DEPENDENCIES}" "${OTB_ARCHIVE_CORE}" "${OTB_ARCHIVE_SAR}" "${OTB_ARCHIVE_FEATURE_EXTRACTION}")
else
    echo "- OTB installer:    ${OTB_INSTALLER}"
    OTB_ARCHIVES=("${OTB_package}")
fi
echo "- OTB version:        ${OTB_VERSION}"
echo "- LIA version:        ${LIA_VERSION}"
echo "- GAMMA_AREA version: ${GAMMA_VERSION}"
echo "- FULL_VERSION:       ${FULL_VERSION}"
echo "- PYTHON_VERSION:     ${PYTHON_VERSION}"

echo ""
echo " => produce:"
for step in "${type_list[@]}" ; do
    if [ -d "${step}" ] ; then
        dest_repository="registry.orfeo-toolbox.org/s1-tiling/s1tiling"
        S1TILING_VERSION="$(sh "${user_dir}/docker/version_name.sh")"
        echo -e "\t- user:\t${dest_repository}/s1tiling:${S1TILING_VERSION}-${os}-otb${OTB_VERSION}"
    else
        echo -e "\t- ${step}:\t${repository}/s1tiling-${step}:${FULL_VERSION}"
    fi
done

_ask_Yes_no "${_colors[cyan]}Continue?" || exit 0
echo -en "${_colors[reset]}"


# ==[ Prepare build environments {{{2
declare -A step_pwds
declare -A step_args
declare -A step_dockerfile

step_args['dev']="--build-arg REPOSITORY=${repository}"
step_args['user']="--build-arg REPOSITORY=${repository}"

step_dockerfile['dev']="$(readlink -f "${root_dir}/${os}/dev/Dockerfile")"
step_dockerfile['user']="$(readlink -f "${root_dir}/${os}/user/Dockerfile")"

# "base" step requires several files
if _belongs base "${type_list[@]}" ; then
    echo ""
    echo "Prepare environment for building 'base' image"
    root_otb_pkg="$(dirname ${OTB_package})"
    if [ -f "${root_otb_pkg}/gdal-config"  ] ; then
        base_wrk_dir="${root_otb_pkg}"
    else
        base_wrk_dir="$(mktemp --directory)"
        _execute cp "${OTB_ARCHIVES[@]}" "${root_dir}/scripts/gdal-config" "${root_dir}/${os}/base/${DOCKERFILE_NAME_base}" "${base_wrk_dir}/"
        if _is_true _install_v9 ; then
            _execute cp "${root_dir}/scripts/excludes.txt" "${root_dir}/scripts/trim-otb-targets.py" "${root_dir}/scripts/post_install.sh" "${base_wrk_dir}/"
        fi
        # register directory removal
        tmp_wrk_dir="${base_wrk_dir}"
        trap _clean_tmp_dir EXIT
    fi
    step_pwds['base']="${base_wrk_dir}"
    step_dockerfile['base']="$(readlink -f "${root_dir}/${os}/base/${DOCKERFILE_NAME_base}")"
    step_args['base']="--build-arg OTB_ORIG=."
    [ -z "${LIA_VERSION}" ] || step_args['base']="${step_args['base']} --build-arg LIA_VERSION=${LIA_VERSION}"
    [ -z "${GAMMA_AREA_VERSION}" ] || step_args['base']="${step_args['base']} --build-arg GAMMA_AREA_VERSION=${GAMMA_AREA_VERSION}"
    tree "${tmp_wrk_dir}"
    echo "  -> Work dir for 'base': '${base_wrk_dir}'"
fi

# "user" step requires 1 file
if _belongs user "${type_list[@]}" ; then
    echo ""
    echo "Prepare environment for building 'user' image"
    root_otb_pkg="$(dirname ${OTB_package})"
    if [ -f "${root_otb_pkg}/gdal-config"  ] ; then
        user_wrk_dir="${root_otb_pkg}"
    else
        if _is_set tmp_wrk_dir ; then
            # Reuse tmp work dir
            user_wrk_dir="${tmp_wrk_dir}"
        else
            user_wrk_dir="$(mktemp --directory)"
            # register directory removal
            tmp_wrk_dir="${user_wrk_dir}"
            trap _clean_tmp_dir EXIT
        fi
        _execute cp "${root_dir}/scripts/s1tiling-requirements.txt" "${root_dir}/scripts/entrypoint.sh" "${root_dir}/${os}/user/Dockerfile" "${user_wrk_dir}/"
    fi
    step_pwds['user']="${user_wrk_dir}"
    step_args['user']=
    if _is_set S1TILING_VERSION ; then
        step_args['user']="--build-arg S1TILING_VERSION=${S1TILING_VERSION}"
    fi
    echo "  -> Work dir for 'user': '${user_wrk_dir}'"
fi

# ==[ Extract Proxy settings     {{{2
_is_set http_proxy  && args+=(--build-arg http_proxy="${http_proxy}")
_is_set https_proxy && args+=(--build-arg https_proxy="${https_proxy}")

# ==[ And build the images!      {{{2
echo ""
echo "Build environments:"
for step in "${type_list[@]}" ; do
    if [ -d "${step}" ] ; then
        # Directories will be taken care of afterward
        continue
    fi
    step_dir="$(readlink -f "${root_dir}/${os}/${step}")"
    _is_set step_pwds["${step}"] && wrk_dir="${step_pwds["${step}"]}" || wrk_dir="${step_dir}"
    echo "* Building ${os}/${step} from ${wrk_dir}"
    (_execute cd "${wrk_dir}" &&
        _execute docker build --no-cache -t "${repository}/s1tiling-${step}:${FULL_VERSION}"  ${step_args["${step}"]} "${args[@]}" -f "${step_dockerfile["${step}"]}" --progress=plain .)
done

# Case of <directory> in type
if _is_set user_dir ; then
    step_dir="$(readlink -f "${user_dir}")"
    wrk_dir="${step_dir}"
    echo "* Building S1Tiling docker from source from ${wrk_dir}"
    args+=(--build-arg "REPOSITORY=${repository}")
    args+=(--build-arg BASE_FULL_VERSION="${FULL_VERSION}")
    args+=(--build-arg "S1TILING_VERSION=${S1TILING_VERSION}")
    # override
    args=($(_replace_in_array CI_COMMIT_SHA= CI_COMMIT_SHA="$(cd "${wrk_dir}" && _git_sha)" "${args[@]}"))
    (_execute cd "${wrk_dir}" &&
        # _execute docker build -t "${dest_repository}/s1tiling:${S1TILING_VERSION}-${OS}-otb${OTB_VERSION}" "${args[@]}" -f "docker/${DOCKERFILE_NAME}" .)
        _execute docker build --no-cache -t "${dest_repository}/s1tiling:${S1TILING_VERSION}-${OS}-otb${OTB_VERSION}" "${args[@]}" -f "${step_dockerfile["${step}"]}" .)
        # _execute docker build -t "${dest_repository}/s1tiling:${S1TILING_VERSION}-${OS}-otb${OTB_VERSION}" "${args[@]}" .)
fi

#docker push registry.orfeo-toolbox.org/s1-tiling/s1tiling-dockers
# }}}1
# vim:set foldmethod=marker:
