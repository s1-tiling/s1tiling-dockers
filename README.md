# s1tiling-dockers

This repository centralizes all the rules for building S1Tiling dockers

```
.
├── fedora --- CANNOT BE PRODUCED see [#3](https://gitlab.orfeo-toolbox.org/s1-tiling/s1tiling-dockers/-/issues/3)
│   ├── base -- root internal dockerfile that takes cares of S1Tiling dependencies
│   ├── dev  -- dockerfile used for testing S1Tiling (Unit Testing, Non regression tests...)
│   └── user -- ready to be used dockerfile for end-users
└── ubuntu
    ├── base -- root internal dockerfile that takes cares of S1Tiling dependencies
    ├── dev  -- dockerfile used for testing S1Tiling (Unit Testing, Non regression tests...)
    └── user -- ready to be used dockerfile for end-users
```

## Building the images

### With the CI

Everytime new commits are pushed, docker images are updated.
The CI takes care of passing a few parameters, among which the following are
mandatory when building both dockers:

- `$OS_VERSION`
- `$PYTHON_VERSION`
- `$OTB_VERSION`
- `$OTB_INSTALLER` (ex.: `"OTB-${OTB_VERSION}-Linux64.run"` or
  `"OTB-${OTB_VERSION}-Linux64-glibc-2.27.run"`)

Plus the following which are mandatory for the `dev` docker

- `$S1TD_VERSION` -- current version of these S1Tiling support dockers
- `FULL_VERSION` (`"${S1TD_VERSION}-${OS}${OS_VERSION}-otb${OTB_VERSION}"`)

The current naming scheme for the dockers is:

- `s1tiling-base:${S1TD_VERSION}-${OS}${OS_VERSION}-otb${OTB_VERSION}`
- `s1tiling-dev:${S1TD_VERSION}-${OS}${OS_VERSION}-otb${OTB_VERSION}`

### With the building script

Drop [OTB binary package](https://www.orfeo-toolbox.org/download/) anywhere on
disk, and execute

```bash
path/to/scripts/build-dockers.sh -os ubuntu --type all /the/path/to/OTB-?.?.?-Linux64.run
```

The script will automatically:

1. Deduce OTB version from the binary package
2. Create the docker images `s1tiling-{conf}:{ubuntu}-otb-{version}` for all
   the configurations: `base`, `dev` and `user`


Parameters:

- `--os`: **`ubuntu`** or <del>`fedora`</del>
- `--type`: **`all`** , or any coma-separated combination of `base`, `dev` and `user`
- complete pathname to OTB binary package
- `repository`: repository where the dockers are expected to be registered
  (default: `registry.orfeo-toolbox.org/s1-tiling/s1tiling-dockers`)

### Manually (outdated)
#### `base` images
Make sure to sure to drop
[OTB binary package](https://www.orfeo-toolbox.org/download/) into `base/`
directories as well as [`scripts/gdal-config`](scripts/gdal-config).

Then execute

```bash
cd ubuntu/base
wget https://www.orfeo-toolbox.org/packages/OTB-7.4.1-Linux64.run
docker build -t s1tiling-base:ubuntu-otb7.4.1 --build-arg OTB_VERSION=7.4.1 .
```

Requirements:

- The version number shall match the release version of OTB.
- The name (tag!) of the produced image needs to match
  `s1tiling-base:{os}-otb{version-M.m.p}` as other images buid procedures will
  depend on it.


#### `dev` images
This step depends on `s1tiling-base:{os}-otb{version-M.m.p}`.

Just execute

```bash
cd ubuntu/dev
docker build -t s1tiling-dev:ubuntu-otb7.4.1 --build-arg OTB_VERSION=7.4.1 .
```

#### `user` images
This step depends on `s1tiling-base:{os}-otb{version-M.m.p}`.

Just execute

```bash
cd ubuntu/user
docker build -t s1tiling-user:ubuntu-otb7.4.1 --build-arg OTB_VERSION=7.4.1 .
```
